"use strict"
var gulp = require('gulp')
var browserify = require('browserify')
var source = require('vinyl-source-stream')
var browserSync = require('browser-sync').create()
var es6ify = require('es6ify')
var nodemon = require('gulp-nodemon')
var model = require("./app/model/model")
let testPopulate = require("./app/test/populate.js")
var BbPromise = require("bluebird")

const base = "./app/"

gulp.task('populate', function() {
    //model.manualSync({force: true})
    testPopulate()
})

gulp.task('clearsync', function() {
    model.sequelize.query('SET FOREIGN_KEY_CHECKS = 0')
    .then(function(){
        return model.sequelize.sync({ force: true });
    })
    .then(function(){
        return model.sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
    })
    .then(function(){
        console.log('Database synchronised.');
    }, function(err){
        console.log(err);
    });
})

gulp.task('clear', function() {
    model.sequelize.query('SET FOREIGN_KEY_CHECKS = 0').then(() => {
        BbPromise.all([
            model.sequelize.dropSchema("matchteam"),
            model.sequelize.dropSchema("matches"),
            model.sequelize.dropSchema("teams"),
            model.sequelize.dropSchema("days")
        ]).then(() => {
            model.sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
            model.manualSync({force: true})
        })
    })
})

gulp.task('browserify', function() {
    return browserify({
        baseDir: base + 'assets/js',
        entries: [base + 'assets/js/main.js']
    })
        .add(es6ify.runtime)
        .transform(es6ify)
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(base + 'public/js/'))
})

gulp.task('nodemon', ['browserify'], function() {
    return nodemon({
        script: base + "index.js",
        ext: "js",
        ignore: "assets/*",
        env: {'NODE_ENV': 'development'}
    })
})

gulp.task('reload', ['browserify'], function() {
    browserSync.reload()
})

gulp.task('watch', ['nodemon'], function() {

    browserSync.init({
        proxy: "localhost:3000",
        browser: "google chrome",
        port: '3001',
        notify: false,
        //tunnel: 'debate',
        ghostMode: false //gettin reaaaal tired of having my page reloaded cus of other people
    })

    var reload = function () {
        gulp.run('reload')
    }

    gulp.watch(base + '**/*.jade').on('change', function() {
        //gulp.run('jade')
        browserSync.reload()
    })

    gulp.watch(base + 'assets/**/*.js').on('change', function() {
        gulp.run('browserify')
        browserSync.reload()
    })
})
