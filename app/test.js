"use strict"
let model = require("./model/model")
let BbPromise = require("bluebird")
let assert = require("assert")

model.manualSync({force: true}, function() {
    let newDate = new Date()
    BbPromise.all([
        model.Team.create({name: "ayylmao high"}),
        model.Match.create({date: newDate})
    ])
    .spread((team, match) => {
        team.addMatch(match)
    })
    .then(() => {
        model.Team.findOne({name: "ayylmao high", include: [model.Match]}).then((team) => {
            console.log(team.matches[0].date)
            assert.equal(team.matches[0].date.toString(), newDate.toString())
        })
    })
})