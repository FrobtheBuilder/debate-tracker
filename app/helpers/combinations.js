"use strict"
let _ = require("lodash")
let BbPromise = require("bluebird")
let model = require("../model/model")

//test if a collection of sets contains a certain set
let collectionContainsSet = function(arr, set) {
    return _.some(arr, setInArray => {
        let has = true
        setInArray.forEach(el => {
            if (!set.has(el))
                has = false
        })
        return has
    })
}

//makes matches from an array of Day instances and an object that looks like
/*
{
    days[n].id: [sets of (2) teams that will be facing off that day]
}
*/
let createMatches = function(days, matchesByDay) {

    //internal function to create a match from a Day and a Set of two Teams
    let createMatch = function(day, pair) {
        return new BbPromise(function(resolve, reject) {
            model.Match.create().then((newMatch) => {
                BbPromise.all([
                    newMatch.setDay(day),
                    newMatch.setTeams(Array.from(pair))
                ]).then(resolve).catch(reject)
            })
        })
    }

    let promises = []
    days.forEach(day => {
        matchesByDay[day.id].forEach(match => {
            promises.push(createMatch(day, match))
        })
    })
    return promises
}

//makes an array of all possible (unique) Sets of two from an array
let pairAll = function(objects) {
    let pairings = []
    for (let obj of objects) {
        for (let otherObj of objects) {
            let objectSet = new Set([obj, otherObj])
            if (!collectionContainsSet(pairings, objectSet) && objectSet.size > 1) {
                pairings.push(objectSet)
            }
        }
    }
    return pairings
}

//*** the good stuff ***
//takes an array of Team instances and Day instances
//creates matches to pair up all teams while making sure no team has more than 2 matches in a day
//returns a promise that resolves when finished
//or rejects if there are not enough days to pair every team.
let matchTeams = function(teams, days) {

    if (days.length < 1) {
        return new BbPromise(function(resolve, reject) {
            reject(new Error("No days at all!"))
        })
    }
    let pairings = pairAll(teams) //going to contain sets of two teams
    let teamDayCounts = {}

    for (let team of teams) {
        teamDayCounts[team.id] = {}
        for (let day of days) {
            if (day) {
                teamDayCounts[team.id][day.id] = 0
            }
            else {
                model.Day.destroy()
                return new BbPromise((res, rej) => {
                    rej(new Error("Not enough days to pair all the teams!"))
                })
            }

        }
    }
    let matchesByDay = {}
    for (let day of days) {
        matchesByDay[day.id] = []
    }
    let error = null
    for (let pair of pairings) {
        let assigned = false
        let currentDayIndex = 0
        let pairArray = Array.from(pair)
        let team1 = pairArray[0]
        let team2 = pairArray[1]
        while (!assigned) {
            if (currentDayIndex > days.length-1) {
                console.log("Should fail!")
                error = new Error("Not enough days!")
                let endPromises = []
                for (let day2 of days) {
                    endPromises.push(day2.destroy())
                }
                for (let team2 of teams) {
                    endPromises.push(team2.destroy())
                }
                return BbPromise.all(endPromises).then(() => {
                    console.log("Where am I???")
                    return BbPromise.reject(new Error("Not enough days to pair everybody!"))
                })
                break
            }
            let currentDay = days[currentDayIndex]
            if (teamDayCounts[team1.id][currentDay.id] < 2 && teamDayCounts[team2.id][currentDay.id] < 2) {
                teamDayCounts[team1.id][currentDay.id]++
                teamDayCounts[team2.id][currentDay.id]++
                matchesByDay[currentDay.id].push(pair)
                assigned = true
            }
            else {
                currentDayIndex++
            }
        }
    }
    let promises = null
    if (!error) {
        let promises = createMatches(days, matchesByDay)
    }
    console.log("hello?")
    return new BbPromise(function(resolve, reject) {
        if (error) {
            reject(error)
        }
        else {
            BbPromise.all(promises).then(resolve).catch(resolve)
        }
    })
}

module.exports = {
    matchTeams
}
