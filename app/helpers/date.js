"use strict"

let moment = require("moment")
let model = require("../model/model")

let getSaturdaysinInterval = function(start, end) {
    let saturdays = []
    let currentDay = start.endOf("week")
    if (start > end) {
        return []
    }
    saturdays.push(currentDay)
    while (currentDay < end && moment(currentDay).add(7, "days") < end) {
        currentDay = moment(currentDay).add(7, "days")
        saturdays.push(currentDay)
    }
    return saturdays
}

let getDateOfFinalDay = function() {
    return model.Day.findAll({order: [["date", "DESC"]]}).then(days => {
        return days[0].date
    })
}

module.exports = {
    getSaturdaysinInterval,
    getDateOfFinalDay
}
