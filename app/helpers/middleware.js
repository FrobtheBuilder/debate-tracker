"use strict"
let logRequest = function(req, res, next) {
    console.log({
        method: req.method,
        url: req.url,
        params: req.params,
        query: req.query,
        body: req.body
    })
    return next()
}

let setUser = function(req, res, next) {
    if (req.session.user) {
        res.locals.user = req.session.user //oh ha ha having that write to app.locals would be BAD.
    }
    else {
        res.locals.user = null
    }
    if (typeof next === "function") {
        return next()
    }
    else {
        return true
    }
}

module.exports = {
    setUser,
    logRequest
}
