"use strict"
//let model = require("../model/model")

let message = {
	render: function(res, type, message) {
		return res.render("message.jade", {
			type,
			message
		})
	}
}

let checkIfAnyRecords = function(model) {
	return model.findAll().then(records => {
		return records.length !== 0
	})
}

module.exports = {
	message,
	checkIfAnyRecords
}
