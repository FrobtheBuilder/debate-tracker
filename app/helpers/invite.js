"use strict"
let _ = require("lodash")
let config = require("../config.json")
let crypto = require("crypto")

let getKey = function(type) {
    let digit1 = _.random(0, 50)
    let digit2 = _.random(0, 50)
    let result = digit1 + digit2

    let cipherString = [digit1, digit2, result, type].join("x")
    let cipher = crypto.createCipher("aes-256-cbc", config.crypto.key)
    cipher.update(cipherString, "utf8", "hex")
    return cipher.final("hex")
}

let verifyKey = function(encryptedKey, type) {
    let decipher = crypto.createDecipher("aes-256-cbc", config.crypto.key)
    try {
        decipher.update(encryptedKey, "hex", "utf8")
    }
    catch (e) {
        return false
    }
    let decryptedKey = decipher.final("utf8")
    let keyArray = decryptedKey.split("x")
    return (( Number(keyArray[0]) + Number(keyArray[1]) == Number(keyArray[2]) ) && keyArray[3] == type)
}

let keyType = function(encryptedKey) {
    let decipher = crypto.createDecipher("aes-256-cbc", config.crypto.key)
    decipher.update(encryptedKey, "hex", "utf8")
    let decryptedKey = decipher.final("utf8")
    let keyArray = decryptedKey.split("x")
    if ( Number(keyArray[0]) + Number(keyArray[1]) == Number(keyArray[2]) ) {
        return keyArray[3]
    }
    else {
        return null
    }
}

module.exports = {
    getKey,
    verifyKey,
    keyType
}
