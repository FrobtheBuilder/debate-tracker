"use strict"
let BbPromise = require("bluebird")
let moment = require("moment")
require("moment-range")

let setWinner = function(winningTeam, match) {
    return match.getWinner().then(oldWinner => {
        if (oldWinner) {
            if (oldWinner.id != winningTeam.id) {
                oldWinner.wins -= 1
                winningTeam.wins += 1
                return BbPromise.all([
                    oldWinner.save(),
                    winningTeam.save(),
                    match.setWinner(winningTeam)
                ])
            }
            else {
                return "That's the team that won already." //will be wrapped up into a promise
            }
        }
        else {
            winningTeam.wins += 1
            return BbPromise.all([
                winningTeam.save(),
                match.setWinner(winningTeam)
            ])
        }
    })
}
let getMatchScorable = function(match) {
    return match.getDay().then(day => {
        let matchDate = day.date
        let currentDate = new Date()
        let difference = moment.range(matchDate, currentDate)
        return ( (difference.diff("days")) < 7 && (difference.diff("days") > -1) )
    })
}

module.exports = {
    setWinner,
    getMatchScorable
}
