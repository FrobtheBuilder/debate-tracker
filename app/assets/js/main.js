let getTeams = function() {
    let teams = $(".team-entry").map(function() {
        return $(this).val()
    })
    return teams
}

$(() => {
    $(".add-team").click(e => {
        let row = $(".first-row").clone().removeClass("first-row")
        row.find("input").val("")
        $(e.target).parent().parent().before(row)
        getTeams()
    })

    $(".submit-teams").click(e => {
        let data = {
            teams: getTeams().toArray(),
            start: $(".start-date").val(),
            end: $(".end-date").val()
        }
        $.ajax({
            method: "POST",
            url: "/schedule/generate",
            data,
            success: function(data2) {
                let data2obj = JSON.parse(data2)
                console.log(data2obj)
                if (data2obj.success) {
                    console.log("Success! We have secured the enemy intelligence.")
                    $(".submit-success").fadeIn().delay(1200).fadeOut()
                }
                else {
                    console.log(data2obj)
                    $(".submit-failure").text("").text("Error: " + data2obj.reason + "").fadeIn().delay(1500).fadeOut()
                }

            }
        })
    })
})
