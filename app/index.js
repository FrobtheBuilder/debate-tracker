"use strict"
let express = require("express")
let app = express()
let config = require("./config.json")
let model = require("./model/model")
let session = require("express-session")
let FileStore = require("session-file-store")(session)
let testPopulate = require("./test/populate.js")
let json = require("express-json")
let bodyParser = require("body-parser")
let middleware = require("./helpers/middleware")

// MIDDLEWARE //
app.use(express.static(__dirname + '/public'))
app.use(session({
        store: new FileStore({path: __dirname + "/sessions"}),
        resave: true,
        saveUninitialized: true,
        secret: "ayy lmao",
    })
)
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(json())

app.set('view engine', 'jade')
app.set("views", __dirname + `/views/`)

app.use(middleware.logRequest)
app.use(middleware.setUser)

// INITIALIZATION //
model.manualSync({force: false})

// ROUTES //
app.use('/schedule', require("./routes/schedule"))

app.get('/', (req, res) => {
    res.render('index.jade', {title: config.name})
})

app.use('/user', require("./routes/user"))

app.listen(config.port)
