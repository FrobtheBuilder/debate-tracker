"use strict"
let express = require("express")
let model = require("../model/model")
let BbPromise = require("bluebird")
let moment = require("moment")
let combinations = require("../helpers/combinations")
let dateHelper = require("../helpers/date")
let routeHelper = require("../helpers/routes")
let msg = require("../helpers/routes").message
let router = express.Router()
let scoreHelper = require("../helpers/score")
let _ = require("lodash")

router.get("/", (req, res) => {
    BbPromise.all([
        model.Day.findAll({ order: [["date", "ASC"]],include: [{all: true}] }),
        model.Match.findAll({ include: [{all: true}] })
    ]).spread((days, matches) => {
        return res.render("schedule", {
            days: days,
            matches: matches
        })
    })
})

router.get("/match/:id", (req, res) => {
    console.log(req.params.id)
    BbPromise.all([
        model.Match.findOne({
            where: {
                id: req.params.id
            },
            include: [ {all: true} ]
        }),
        model.Day.findAll({
            include: [ {all: true} ]
        }),
        model.Match.findAll({
            include: [{all: true}]
        })
    ]).spread((match, days, allMatches) => {
        let eligibleDays = []

        for (let day of days) {
            let hits = match.teams.map(x => 0)
            for (let oneMatch of allMatches) {
                if (day.matches.some(m => m.id === oneMatch.id)) {
                    for (let ti = 0; ti < match.teams.length; ti++) {
                        if (oneMatch.teams.some(t => t.id == match.teams[ti].id)) {
                            hits[ti]++
                        }
                    }
                }
            }
            if (!hits.some(h => h >= 2)) {
                eligibleDays.push(day)
            }
        }

        if (req.session.user) {
            if (req.session.user.privilege == "ref") {
                scoreHelper.getMatchScorable(match).then(scorable => {
                    return res.render("match.jade", {match, scorable})
                })
            }
            else if (req.session.user.privilege == "sref") {
                return res.render("match.jade", {match, scorable: true, eligibleDays})
            }
        }
        else {
            return res.render("match.jade", {match, scorable: false})
        }
    })
})

router.get("/move/:matchId/:dayId", (req, res) => {
    BbPromise.all([
        model.Day.findOne({where: {id: req.params.dayId}}),
        model.Match.findOne({where: {id: req.params.matchId}})
    ]).spread((day, match) => {
        return match.setDay(day)
    }).then(() => {
        return msg.render(res, "Success", "Moved match!")
    }).catch(e => {
        return msg.render(res, "Error", "Nope, Refused,  Didn't work, Failed: " + JSON.stringify(e))
    })
})

router.post("/match/:id/score", (req, res) => {
    let winningTeamID = Object.keys(req.body)[0]
    model.Match.findOne({where: {id: req.params.id}})
    .then(scoreHelper.getMatchScorable)
    .then(scorable => {
        let canScore = (
            req.session.user && (
                (req.session.user.privilege == "sref") ||
                (req.session.user.privilege == "ref" && scorable)
            )
        )
        if (!canScore) {
            msg.render(res, "Error", "Match is not scorable by you.")
        }
        BbPromise.all([
            model.Team.findOne({where: {id: winningTeamID}}),
            model.Match.findOne({where: {id: req.params.id}})
        ]).spread((team, match) => {
            return scoreHelper.setWinner(team, match)
        }).then(whateverTheHell => {
            return msg.render(res, "Success", "Match score updated!")
        }).catch(e => {
            return msg.render(
                res,
                "Error",
                "Something went wrong: " + JSON.stringify(e)
            )
        })
    })
})

router.get("/create", (req, res) => {
    //probably not exactly secure but who cares right now.
    //it could be fixed easily enough
    if (req.session.user && req.session.user.privilege == "sref") {
        return res.render("createschedule.jade")
    }
    else {
        return msg.render(res, "Error", "Insufficient privileges to create schedule.")
    }
})

router.post("/generate", (req, res) => {
    console.log(req.body)
    let start = moment(new Date(req.body.start))
    let end = moment(new Date(req.body.end))
    if (dateHelper.getSaturdaysinInterval(start, end).length === 0) {
        return res.json({success: false, reason: "Bad date range!"})
    }
    if (req.body.teams.length < 1) {
        return res.json({success: false, reason: "No teams!"})
    }
    if (req.body.teams.some(x => x === "")) {
        return res.json({success: false, reason: "There's at least one blank team field!"})
    }
    let saturdays = dateHelper.getSaturdaysinInterval(start, end).map(x => x.toDate())
    routeHelper.checkIfAnyRecords(model.Day).then(any => {
        if (any) {
            console.log({success: false, reason: "Schedule already created."})
            return res.json({success: false, reason: "Schedule already created."})
        }
        BbPromise.all([
            model.Day.bulkCreate(saturdays.map(d => {return {date: d}})),
            model.Team.bulkCreate(req.body.teams.map(t => {return {name: t}}))
        ]).then(() => {
            BbPromise.all([
                model.Team.findAll({ include: [ model.Match ] }),
                model.Day.findAll({ include: [ model.Match ] })
            ]).spread((teams, days) => {
                combinations.matchTeams(teams, days)
                .then(() => {
                    return res.json({success: "true"})
                }, (e) => {
                    return res.json({success: false, reason: e.message})
                })
            })
        })
    })
})

router.post("/addday", (req, res) => {
    dateHelper.getDateOfFinalDay().then(date => {
        let dateOfNewDay = moment(date).add(7, "days")
        model.Day.create({date: dateOfNewDay.toDate()}).then(day => {
            msg.render(res, "Success", "Added a new Day.")
        })
    })
})

router.get("/leaderboard", (req, res) => {
    model.Team.findAll({order: [["wins", "DESC"]]}).then(teams => {
        res.render("leaderboard.jade", {teams})
    })
})

module.exports = router
