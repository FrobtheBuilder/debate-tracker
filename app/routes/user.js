"use strict"
let model = require("../model/model")
let express = require("express")
let router = express.Router()
let config = require("../config.json")
let crypto = require("crypto")
let routeHelper = require("../helpers/routes")
let msg = require("../helpers/routes").message
let middleware = require("../helpers/middleware")
let invite = require("../helpers/invite")

router.get("/register", (req, res) => {
	let firstUser = false
	routeHelper.checkIfAnyRecords(model.User).then(areThere => {
		res.render('register.jade', {firstUser: !areThere})
	})
})

router.post("/register", (req, res) => {
	routeHelper.checkIfAnyRecords(model.User).then(areThere => {
		let keyRequired = areThere
		let userType = ""

		if (!(req.body.name && req.body.passwd)) {
			return res.render("message", {message: "Please fill in both fields."})
		}
		console.log(keyRequired)
		if (keyRequired) {
			if (req.body.key &&
			   (invite.verifyKey(req.body.key,"ref") || invite.verifyKey(req.body.key,"sref"))) {
				userType = invite.keyType(req.body.key)
			}
			else {
				return msg.render(res, "Error", "Bad key!")
			}
		}
		else {
			userType = "sref"
		}

		let pwhash = crypto.createHash("md5")
		pwhash.update(config.crypto.salt + req.body.passwd, "utf8")
		model.User.create({
			name: req.body.name,
			pwHash: pwhash.digest("hex"),
			privilege: userType
		}).then(user => {
			req.session.user = user
			middleware.setUser(req, res)
			return msg.render(res, "Success", "User created!")
		}).catch(error => {
			return msg.render(
				res, "Error", "Something went wrong." + JSON.stringify(error)
			)
		})
	})
})


router.get("/login", (req, res) => {
    res.render('memberlogin.jade')
})

router.post("/login", (req, res) => {
	if (!(req.body.name && req.body.passwd)) {
		return msg.render(res, "Error", "Please fill in both fields!")
	}

	let pwhash = crypto.createHash("md5")
	pwhash.update(config.crypto.salt + req.body.passwd, "utf8")

	model.User.findOne({
		where: {
			name: req.body.name,
			pwHash: pwhash.digest("hex")
		}
	}).then(user => {
		if (!user) {
			return msg.render(res, "Error", "Incorrect username or password.")
		}
		else {
			req.session.user = user
			middleware.setUser(req, res)
			return res.render("message.jade", {
				type: "Success",
				message: "You are now logged in."
			})
		}
	}).catch(err => {
		return msg.render(res, "Error", JSON.stringify(err))
	})
})

router.get("/logout", (req, res) => {
	req.session.user = null
	middleware.setUser(req, res)
	return msg.render(res, "Success", "You have been logged out.")
})

router.get("/:id", (req, res) => {
	model.User.findOne({where: {id: req.params.id}}).then(targetUser => {
		let canMakeKey = (req.session.user &&
			(req.session.user.id == req.params.id) &&
			(req.session.user.privilege == "sref"))
		res.render("user.jade", {targetUser, canMakeKey})
	})
})

router.post("/getkey", (req, res) => {
	let key = ""
	if (!req.session.user || req.session.user.privilege !== "sref") {
		return msg.render(res, "Error", "Insufficient Privileges.")
	}
	let type = Object.keys(req.body)[0]

	if (!type) {
		return msg.render(res, "Error", "Please select a type.")
	}

	key = invite.getKey(type)
	return msg.render(res, "Success",
		"Give this code to the person making the account: <br>" + key)
})

module.exports = router
