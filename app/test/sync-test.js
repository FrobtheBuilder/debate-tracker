"use strict"
let model = require("../model/model")
let BbPromise = require("bluebird")
let _ = require("lodash")

model.manualSync({force: true}, function() {
    BbPromise.all(
        [
            model.Team.bulkCreate([
                {name: "Team 1"},
                {name: "Team 2"},
                {name: "Team 3"}
            ]),
            model.Match.bulkCreate([
                {date: new Date()},
                {date: new Date()},
                {date: new Date()},
                {date: new Date()},
                {date: new Date()},
                {date: new Date()}
            ])
        ]
    ).then(() => {
        BbPromise.all([
            model.Team.findOne({name: "Team 1", include: [ model.Match ]}),
            model.Match.findOne({id: 1})
        ])
        .spread((team, match) => {
            team.addMatch(match).then(() => {
                team.save().then(() => {
                    model.Team.findOne({name: "Team 1", include: [ model.Match ]}).then(console.log)
                })
            })
        })
    })
})