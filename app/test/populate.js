"use strict"
let combinations = require("../helpers/combinations")
let model = require("../model/model")
let BbPromise = require("bluebird")
let _ = require("lodash")

module.exports = function() {
    model.manualSync({force: true}, function() {
        BbPromise.all(
            [
                model.Team.bulkCreate([
                    {name: "Team 1"},
                    {name: "Team 2"},
                    {name: "Team 3"},
                    {name: "Team 4"},
                    {name: "Team 5"},
                    {name: "Team 6"},
                    {name: "Team 7"},
                    {name: "Team 8"},
                    {name: "Team 9"},
                    {name: "Team 10"}
                ]),
                model.Day.bulkCreate([
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()},
                    {date: new Date()}
                ])
            ]
        ).then(() => {
            BbPromise.all([
                model.Team.findAll({ include: [ model.Match ] }),
                model.Day.findAll({ include: [ model.Match ] })
            ])
            .spread((teams, days) => {
                combinations.matchTeams(teams, days).then(console.log)
            })

        //there is no chance that this will work
        //just don't, bro
        //don't
        }).then(() => {
            return new BbPromise(function(resolve, reject) {
                resolve()
            })
        }).catch(e => {
            return new BbPromise(function(resolve, reject) {
                reject()
            })
        })
    })
}
