"use strict"
let mysql = require("mysql")

let connection =  {
    mysql: {},
    connect: function(config) {
        this.mysql = mysql.createConnection(config)
    }
}

class Model {
    constructor(tableName) {
        this.tableName = tableName
    }

    where(select, glue) {
        glue = " and "
        let where = []
        let values = []

        for (var n of Object.keys(select)) {
            where.push(connection.mysql.escapeId(n) + "=?")
            values.push(select[n])
        }

        return { where: where.join(glue), values }
    }

    get(select) {
        var s = this.where(select)
        return new Promise(function(resolve, reject) {
            var query = connection.mysql.query(
                "select * from `" + this.tableName + "` where " + s.where + " limit 1",
                s.values,
                function(err, results) {
                    if (err)
                        reject(err)
                    else
                        resolve(results)
                }
            )
            console.log(query.sql)
        })
    }

    create(attributes) {
        var keys = []
        var values = []
        var places = []
        for (var k of Object.keys(attributes)) {
            keys.push(k)
            places.push("?")
            values.push(attributes[k])
        }
        keys.join(",")
        places.join(",")
        return new Promise((resolve, reject) => {
            var query = connection.mysql.query(
                "insert into " + 
                connection.mysql.escapeId(this.tableName) + 
                " (" + keys + ") values (" + places + ") ", values,
                function(err, results) {
                    if (err)
                        reject(err)
                    else
                        resolve(results)
                }
            )
            console.log(query.sql)
        })
    }
}

class UserModel extends Model {
    constructor(tableName) {
        super(tableName)
    }
}

class TeamModel extends Model {
    constructor(tableName) {
        super(tableName)
    }

    create(name) {
        return new Promise((resolve, reject) => {
            super.create({name: name}).then((results) => {
                super.get({id: results.insertId})
                .then((record) => {
                    results.team = record
                    resolve(results)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        })
    }
}

let user = new UserModel("users")
let team = new TeamModel("teams")

/*
let user = new Model()
user.get = function(select) {
    return Model.prototype.get.call(this, "users", select)
}
*/
/*
let team = new Model()
team.create = function(name) {
    return new Promise((resolve, reject) => {
        Model.prototype.create.call(this, "teams", {name: name, wins: 3}).then((results) => {
            team.get({id: results.insertId})
            .then((record) => {
                results.team = record
                resolve(results)
            })
            .catch((err) => {
                reject(err)
            })
        })
    })
}

team.get = function(select) {
    return Model.prototype.get.call(this, "teams", select)
}

team.getDebates = function() {

}

*/

module.exports = {
    connection,
    user,
    team
}


/*
let util = {
    where: function(select, glue) {
        glue = " and "
        let where = []
        let values = []

        for (var n of Object.keys(select)) {
            where.push(connection.mysql.escapeId(n) + "=?")
            values.push(select[n])
        }

        return { where: where.join(glue), values }
    },
    get: function(table, select) {
        var s = util.where(select)
        return new Promise(function(resolve, reject) {
            var query = connection.mysql.query(
                "select * from `" + table + "` where " + s.where + " limit 1",
                s.values,
                function(err, results) {
                    console.log(query.sql)
                    if (err)
                        reject(err)
                    else
                        resolve(results)
                }
            )
        })
    },
    create: function(table, attributes) {
        var keys = []
        var values = []
        var places = []
        for (var k of Object.keys(attributes)) {
            keys.push(k)
            places.push("?")
            values.push(attributes[k])
        }
        keys.join(",")
        places.join(",")
        return new Promise((resolve, reject) => {
            var query = connection.mysql.query(
                "insert into " + 
                connection.mysql.escapeId(table) + 
                " (" + keys + ") values (" + places + ") ", values,
                function(err, results) {
                    if (err)
                        reject(err)
                    else
                        resolve(results)
                }
            )
            console.log(query)
        })
    }
}
*/

/*
let user = {
    get: function(select) {
        return util.get("users", select)
    }
}
*/
/*
let team = {
    create: function(name) {
        return new Promise((resolve, reject) => {
            util.create("teams", {name: name, wins: 3}).then((results) => {
                team.get({id: results.insertId})
                .then((record) => {
                    results.team = record
                    resolve(results)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        })
    },
    get: function(select) {
        return util.get("teams", select)
    },
    getDebates: function() {

    }
}
*/