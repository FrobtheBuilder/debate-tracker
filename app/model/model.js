"use strict"
let config = require("../config.json")
let db = config.db
let Sequelize = require("sequelize")

let sequelize = new Sequelize(db.database, db.user, db.password, {
    host: db.host,
    dialect: "mariadb"
})

let User = sequelize.define("user", {
    name: {
        type: Sequelize.STRING
    },
    pwHash: {
        type: Sequelize.STRING
    },
    privilege: {
        type: Sequelize.ENUM("none", "ref", "sref")
    }
})

let Team = sequelize.define("team", {
    name: {
        type: Sequelize.STRING
    },
    wins: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    }
})


let Match = sequelize.define("match", {

})

let Day = sequelize.define("day", {
    date: {
        type: Sequelize.DATE
    }
})

Day.hasMany(Match)
Match.belongsTo(Day)
Match.belongsToMany(Team, {through: "MatchTeam"})
Team.belongsToMany(Match, {through: "MatchTeam"})
Match.belongsTo(Team, {as: "winner"})

let manualSync = function(options, callback) {
    sequelize.query('SET FOREIGN_KEY_CHECKS = 0').then(() => {
        sequelize.sync(options).then(() => {
            sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
            if (options.force) {
                //sequelize.dropSchema("matchteam")
                //    .catch(e => console.log("Matchteam is already dropped. Everything is okay."))
            }
            if (callback)
                sequelize.sync().then(callback)
            else
                sequelize.sync()
        })
    })
}

module.exports = {
    sequelize,
    User,
    Team,
    Match,
    Day,
    manualSync
}
